﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class AppEvents
{
  public static string ReservationsLoaded = "ReservationsLoaded";
  public static string ChangeHeader = "ChangeHeader";
  public static string ShowSeats = "ShowSeats";
}
