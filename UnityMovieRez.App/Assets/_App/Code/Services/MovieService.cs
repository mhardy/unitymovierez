using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;
using System.Linq;
using UnityEngine.UI;
using UnityMovieRez.Tools;

public class MovieService : Singleton<MovieService>
{
  public GameObject MovieDisplay; // temp until we add UI Toolkit

  public List<Movie> Movies = new List<Movie>();
  public List<Reservation> Reservations = new List<Reservation>();
  public bool IsLoaded { get { return _moviesTotal > 0 && _moviesLoaded == _moviesTotal; } }

  private int _moviesLoaded = 0;
  private int _moviesTotal = 0;

#if UNITY_EDITOR
  private const string _apiUrl = "http://localhost:7071/api/{0}?code=ELVJlPgc4tRuf/cUnvaLzIlzZrvnz5J7rF7Ujk7gOuhVSPzz1kD9hQ==";
#else
  private const string _apiUrl = "https://unitymovierezapi.azurewebsites.net/api/{0}?code=ELVJlPgc4tRuf/cUnvaLzIlzZrvnz5J7rF7Ujk7gOuhVSPzz1kD9hQ==";
#endif

  //=====================================================================================================
  /// <summary>
  /// Load up the reservations and movies once we've authenticated.
  /// </summary>
  public void Initialize()
  {
    GetReservations();
    LoadMovies();
  }
  //=====================================================================================================
  public void MakeReservation(int movieId, DateTime date, TimeSpan time, int seatId)
  {
    string playerId = AuthenticationManager.Instance.PlayerId;
    Log.Info($"MovieService, MakeReservation, player:{playerId}, movie:{movieId}");

    string resUrl = string.Format(_apiUrl, "MakeReservation");
    Reservation res = new Reservation
    {
      MovieId = movieId,
      PlayerId = playerId,
      TicketDate = date.Date,
      TicketTime = time,
      SeatId = seatId
    };

    var json = JsonConvert.SerializeObject(res);

    Log.Info($"Res Url:{resUrl}");
    StartCoroutine(NetworkService.Instance.PostJson<PostResponse>(resUrl, json, (response) =>
    {
      if( response.IsError )
      {
        Log.Error($"making resevation, {response.Message}");
        Messenger<string>.Broadcast(AppEvents.ChangeHeader, "Reservation Failed");
      }
      else
      {
        Log.Info($"Reservation made, id:{response.Id}");

        // just add directly so we don't have to round trip to reload
        Reservations.Add(res);
        Messenger.Broadcast(AppEvents.ReservationsLoaded); // update badge and movie list
        Messenger<string>.Broadcast(AppEvents.ChangeHeader, "Reservation Confirmed!");
      }
    }));
  }
  //=====================================================================================================
  /// <summary>
  /// Get all reservations and send the requested appEvent message. This defaults to ReservationsLoaded.
  /// </summary>
  public void GetReservations(string appEvent = null)
  {
    string resUrl = string.Format(_apiUrl, "GetAllReservations");

    Log.Info($"Res Url:{resUrl}");
    StartCoroutine(NetworkService.Instance.GetJson<List<Reservation>>(resUrl, (resz) =>
    {
      Reservations.Clear();
      foreach( var res in resz )
      {
        //Log.Info($"-- res:{res.ReservationId}, seat:{res.SeatId}");
        Reservations.Add(res);
      }
      Messenger.Broadcast(appEvent ?? AppEvents.ReservationsLoaded);
    }));
  }
  //=====================================================================================================
  /// <summary>
  /// Pull down all the movies from our database via our Azure API, then get any missing posters
  /// from the tmdb site and cache them to disk.
  /// </summary>
  private void LoadMovies()
  {
    // this will be killed by the MainScreen.OnEnable which watches for movies to finish loading
    UIController.Instance.ShowLoadingDialog();

    string moviesUrl = string.Format(_apiUrl, "GetAllMovies");
    StartCoroutine(NetworkService.Instance.GetJson<List<Movie>>(moviesUrl, (movies) =>
    {
      _moviesTotal = movies.Count;
      foreach( var m in movies )
      {
        string path = GetPosterFilePath(m.poster_path);
        //Log.Info($"- Movie:{m.id}, Title:{m.title}, Poster:{m.poster_path}, local:{path}");
        if( !File.Exists(path) )
          // download and cache it
          StartCoroutine(NetworkService.Instance.GetTextureAndCache(GetPosterUrl(m.poster_path), path, (texture) =>
            {
              m.Poster = texture;
              Movies.Add(m);
              _moviesLoaded++;
            }));
        // else load it from cache
        else
        {
          var bytes = File.ReadAllBytes(path);
          var texture = new Texture2D(2, 2);
          texture.LoadImage(bytes);
          //Log.Info($"text: {texture.width}x{texture.height}");
          var sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
          m.Poster = sprite;
          Movies.Add(m);
          _moviesLoaded++;
        }
      }
    }));
  }
  //=====================================================================================================
  /// <summary>
  /// Get the local cache path for the poster.
  /// </summary>
  /// <param name="posterPath"></param>
  /// <returns></returns>
  private string GetPosterFilePath(string posterPath)
  {
    string slash = posterPath.StartsWith("/") ? "" : "/";

    return $"{Application.persistentDataPath}/Files{slash}{posterPath}";
  }
  //----------------------------------------------------------------------------------------------------
  /// <summary>
  /// Get the TMDB url for the poster.
  /// </summary>
  /// <param name="posterPath"></param>
  /// <returns></returns>
  private string GetPosterUrl(string posterPath)
  {
    string slash = posterPath.StartsWith("/") ? "" : "/";
    return $"https://image.tmdb.org/t/p/w154{slash}{posterPath}";
  }
  //=====================================================================================================
  public Movie GetPendingMovie()
  {
    return Movies.Where(a => a.id == UIController.Instance.PendingReservation.MovieId).FirstOrDefault();
  }
}
