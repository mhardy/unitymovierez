using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity.Services.Authentication;
using Unity.Services.Core;
using UnityEngine;
using UnityMovieRez.Tools;

public class GameServices : MonoBehaviour
{
  //=====================================================================================================
  async Task Awake()
  {
    Log.Info($"GameServices initializing");

    await UnityServices.InitializeAsync();
    await AuthenticationManager.Instance.Login();

    // a pending res is always for the current player. set once we have it so can add to the Reservations list later.
    UIController.Instance.PendingReservation.PlayerId = AuthenticationManager.Instance.PlayerId;

    // Load up our movies and resz now that we have a PlayerId
    MovieService.Instance.Initialize();

    UIController.Instance.ShowDebugText(AuthenticationManager.Instance.PlayerId?.Substring(0, 5));
  }
}
