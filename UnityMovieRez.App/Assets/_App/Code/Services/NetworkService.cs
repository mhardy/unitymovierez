using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;
using UnityMovieRez.Tools;

public class NetworkService : Singleton<NetworkService>
{
  //=====================================================================================================
  public IEnumerator PostJson<T>(string url, string jsonData, Action<T> callback)
  {
    Log.Info($"Posting json to web");
    byte[] bytes = Encoding.UTF8.GetBytes(jsonData);
    using( UnityWebRequest www = UnityWebRequest.Put(url, bytes) )
    {
      www.SetRequestHeader("Content-Type", "application/json");
      yield return www.SendWebRequest();

      if( www.result != UnityWebRequest.Result.Success )
      {
        Log.Error($"{www.result}, {www.downloadHandler.error}");
        yield break;
      }

      var json = www.downloadHandler.text;
      Log.Info($"json:{json}");
      T model = JsonConvert.DeserializeObject<T>(json);
      callback(model);
    }
  }

  //=====================================================================================================
  public IEnumerator GetJson<T>(string url, Action<T> callback) where T : class
  {
    Log.Info($"Loading json from web");
    using( UnityWebRequest www = UnityWebRequest.Get(url) )
    {
      yield return www.SendWebRequest();

      if( www.result != UnityWebRequest.Result.Success )
      {
        Log.Error($"{www.result}, {www.downloadHandler.error}");
        yield break;
      }
      var json = www.downloadHandler.text;
      T model = JsonConvert.DeserializeObject<T>(json);
      callback(model);
    }
  }
  //=====================================================================================================
  /// <summary>
  /// Pull down the movie poster and cache it to disk.
  /// </summary>
  /// <param name="url">Url from tmdb where the poster is.</param>
  /// <param name="path">File path where to cache the poster.</param>
  /// <param name="callback">Sends the texture back if ya want it.</param>
  /// <returns></returns>
  public IEnumerator GetTextureAndCache(string url, string path, Action<Sprite> callback)
  {
    Log.Info($"Loading texture from web, url:{url}, path:{path}");
    using( UnityWebRequest www = UnityWebRequestTexture.GetTexture(url) )
    {
      yield return www.SendWebRequest();

      if( www.result != UnityWebRequest.Result.Success )
      {
        Log.Error($"{www.result}, {www.downloadHandler.error}");
        yield break;
      }

      FileInfo fi = new FileInfo(path);
      Directory.CreateDirectory(fi.DirectoryName);
      Debug.Log($"path:{path}, dir:{fi.DirectoryName}");

      File.WriteAllBytes(path, ((DownloadHandlerTexture)www.downloadHandler).data);

      var texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
      var sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
      callback(sprite);
    }
  }
}
