using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity.Services.Authentication;
using UnityEngine;
using UnityMovieRez.Tools;

/// <summary>
/// This is at the top of the Script Execution Order so it runs asap.
/// </summary>
public class AuthenticationManager : Singleton<AuthenticationManager>
{
  public string PlayerId { get { return AuthenticationService.Instance.PlayerId; } }

  //=====================================================================================================
  /// <summary>
  /// Login to Unity GameServices anonymously so we can maintain state with their PlayerId.
  /// </summary>
  /// <returns></returns>
  public async Task Login()
  {
    try
    {
      if( AuthenticationService.Instance.IsSignedIn )
      {
        Log.Error($"AuthManager, already SignedIn, you must sign out first!");
        return;
      }

      await AuthenticationService.Instance.SignInAnonymouslyAsync();
      Log.Info($"AuthManager PlayerID: {AuthenticationService.Instance.PlayerId}");
    }
    catch( Exception ex )
    {
      Log.Error("AuthenticationManager.Login", ex);
    }
  }
}
