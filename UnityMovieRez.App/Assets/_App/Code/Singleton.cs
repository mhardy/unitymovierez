﻿using System.Diagnostics;
using UnityEngine;
using UnityMovieRez.Tools;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
	private static T _instance;

	private static object _lock = new object();

	public static T Instance
	{
		get
		{
			if( applicationIsQuitting )
				return null;

			lock( _lock )
			{
				if( _instance == null )
				{
					_instance = (T)FindObjectOfType(typeof(T));

					if( FindObjectsOfType(typeof(T)).Length > 1 )
						Log.Error($"Singleton, more than 1 singleton exists for {typeof(T)}.");

					if( _instance == null )
						Log.Error($"Singleton, an instance of {typeof(T)} is needed, but not found, make sure you put it on a game object");
				}

				return _instance;
			}
		}
	}

	private static bool applicationIsQuitting = false;
	public virtual void OnDestroy()
	{
		applicationIsQuitting = true;
	}
}