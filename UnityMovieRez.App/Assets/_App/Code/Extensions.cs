﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class Extensions
{
  //=====================================================================================================
  public static string Shorten(this string text, int length)
  {
    return text.Length > length ? $"{text.Substring(0, length)}..." : text;
  }
  //=====================================================================================================
  /// <summary>
  /// Remove the timezone data so serialization doesn't change it. Else if a device is in a different
  /// timezone that'll get converted to local time by JsonConvert and possibly change the date.
  /// </summary>
  /// <param name="date"></param>
  /// <returns></returns>
  public static DateTime RemoveTimeZone(this DateTime date)
  {
    return new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, DateTimeKind.Unspecified);

  }
}
