using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movie
{
  public int id { get; set; }
  public string title { get; set; }
  public string overview { get; set; }
  public decimal? popularity { get; set; }
  public string poster_path { get; set; }
  public int? seats { get; set; }
  public Sprite Poster { get; set; }
  /// <summary>
  /// Used to flag if the current user has a reservation for this movie.
  /// </summary>
  public bool Reserved { get; set; }
}
