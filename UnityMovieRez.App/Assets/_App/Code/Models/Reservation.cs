using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reservation
{
  public int ReservationId { get; set; }
  public string PlayerId { get; set; }
  public int MovieId { get; set; }
  public DateTime TicketDate { get; set; }
  public TimeSpan TicketTime { get; set; }
  public int SeatId { get; set; }
  public DateTime DateReserved { get; set; }
}

