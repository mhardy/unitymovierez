using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UIElements;
using UnityMovieRez.Tools;

public class UIController : Singleton<UIController>
{
  [SerializeField] private MainScreen _mainScreen;
  [SerializeField] private VisualTreeAsset _seatsScreenTemplate;
  [SerializeField] private VisualTreeAsset _seatTemplate;

  public Reservation PendingReservation = new Reservation();

  // the PendingRes date can change to show different seating charts, this keeps our currently selected date
  private DateTime _selectedDate;
  public DateTime SelectedDate { get { return _selectedDate.Date; } }

  private ListView _movieList;
  private Label _badgeText;
  private Button _profileButton;
  private VisualElement _footer;
  private VisualElement _body;
  private Button _datesButton;
  private Label _headerMessage;
  private VisualElement _datesDialog;
  private VisualElement _confirmDialog;
  private VisualElement _loadingDialog;
  private Label _debugText;

  private VisualElement _root;

  //=====================================================================================================
  private void OnEnable()
  {
    _root = _mainScreen.GetComponent<UIDocument>().rootVisualElement;

    _body = _root.Q<VisualElement>("Body");
    _movieList = _root.Q<ListView>("MovieList");
    _footer = _root.Q<VisualElement>("Footer");
    _badgeText = _root.Q<Label>("BadgeText");
    _profileButton = _root.Q<Button>("ProfileButton");
    _headerMessage = _root.Q<Label>("HeaderMessage");
    _confirmDialog = _root.Q<VisualElement>("ConfirmDialog");
    _loadingDialog = _root.Q<VisualElement>("LoadingDialog");
    _datesDialog = _root.Q<VisualElement>("DatesDialog");
    _debugText = _root.Q<Label>("DebugText");

    _datesButton = _root.Q<Button>("DatesButton");
    _datesButton.clicked += OnDateButtonClicked;

    _profileButton = _root.Q<Button>("ProfileButton");
    _profileButton.clicked += OnProfileClicked;

    AddDates();

    _confirmDialog.style.display = DisplayStyle.None;
    _loadingDialog.style.display = DisplayStyle.None;
    _datesDialog.style.display = DisplayStyle.None;

    // watch for our resz to finish loading then set our badge
    Messenger.AddListener(AppEvents.ReservationsLoaded, ReservationsUpdated);
    Messenger<string>.AddListener(AppEvents.ChangeHeader, SetHeader);
    Messenger.AddListener(AppEvents.ShowSeats, ShowSeatsScreen);

    // link up our dialog buttons
    _confirmDialog.Q<Button>("OkButton").clicked += SubmitReservation;
    _confirmDialog.Q<Button>("CancelButton").clicked += CancelReservation;
  }
  //----------------------------------------------------------------------------------------------------
  private void OnDisable()
  {
    Messenger.RemoveListener(AppEvents.ReservationsLoaded, ReservationsUpdated);
    Messenger<string>.RemoveListener(AppEvents.ChangeHeader, SetHeader);
    Messenger.RemoveListener(AppEvents.ShowSeats, ShowSeatsScreen);

    // remove so they don't link up twice
    _confirmDialog.Q<Button>("OkButton").clicked -= SubmitReservation;
    _confirmDialog.Q<Button>("CancelButton").clicked -= CancelReservation;
  }
  //=====================================================================================================
  public void ShowDebugText(string text, bool show = true)
  {
    _debugText.style.display = show ? DisplayStyle.Flex : DisplayStyle.None;
    _debugText.text = text;
  }
  //=====================================================================================================
  /// <summary>
  /// Header date button clicked to show date picker
  /// </summary>
  private void OnDateButtonClicked()
  {
    Log.Info($"OnDateButtonClicked");
    _datesDialog.style.display = _datesDialog.style.display == DisplayStyle.Flex ? DisplayStyle.None : DisplayStyle.Flex;
    _datesDialog.BringToFront();
  }
  //=====================================================================================================
  /// <summary>
  /// Add dates to the date picker
  /// </summary>
  private void AddDates()
  {
    DateTime start = DateTime.Now.AddDays(-1);
    DateTime end = start.AddDays(8);
    // strip out the TZ so serialization doesn't change it
    var dates = Enumerable.Range(0, 1 + end.Subtract(start).Days).Select(offset => start.AddDays(offset).Date.RemoveTimeZone()).ToList();
    foreach( var date in dates )
    {
      var b = new Button();
      b.RegisterCallback<ClickEvent, DateTime>(OnDateClicked, date);
      b.AddToClassList("date-item-button");
      b.text = $"{date:ddd M/dd}";
      _datesDialog.Add(b);
    }
    OnDateClicked(null, dates[1]); // today
  }
  //=====================================================================================================
  /// <summary>
  /// A date in the dates dialog was clicked.
  /// </summary>
  private void OnDateClicked(ClickEvent evt, DateTime date)
  {
    Log.Info($"UIController.OnDateClicked, {evt}, {date}");
    _selectedDate = PendingReservation.TicketDate = date;
    
    _datesButton.text = $"{date:ddd M/dd}";
    _datesDialog.style.display = DisplayStyle.None;
  }
  //=====================================================================================================
  private void OnProfileClicked()
  {
    Log.Info($"UIController, OnProfileClicked");

    Messenger<string>.Broadcast(AppEvents.ChangeHeader, "Select a Movie");
    var seatsList = _body.Q<VisualElement>("SeatsScreen");

    // if we're in the seats screen, go back to the main menu
    if( seatsList != null )
    {
      _body.Add(_movieList);
      _body.Add(_footer);
      _body.Remove(seatsList);
    }
  }
  //=====================================================================================================
  /// <summary>
  /// Set the number of reservations for this user or hide the badge if none. Then refresh the movie list
  /// to update the color of any movies with new reservations.
  /// </summary>
  private void ReservationsUpdated()
  {
    var cnt = MovieService.Instance.Reservations.Where(a => a.PlayerId == AuthenticationManager.Instance.PlayerId).Count();
    Log.Info($"SetBadge, resz:{cnt}");
    if( cnt > 0 )
      _badgeText.parent.style.display = DisplayStyle.Flex;
    else
      _badgeText.parent.style.display = DisplayStyle.None;

    _badgeText.text = cnt.ToString();

    // to color orange any new reservations
    _movieList.RefreshItems();
  }
  //=====================================================================================================
  private void SetHeader(string message)
  {
    Log.Info($"SetHeader, {message}");
    _headerMessage.text = message;
  }
  //=====================================================================================================
  /// <summary>
  /// Set fields to collected values and show dialog
  /// </summary>
  public void ShowConfirmDialog()
  {
    _confirmDialog.style.display = DisplayStyle.Flex;
    _confirmDialog.BringToFront();

    var movie = MovieService.Instance.Movies.Where(a => a.id == PendingReservation.MovieId).FirstOrDefault();

    _confirmDialog.Q<Label>("MovieLabel").text = $"{movie?.title.Shorten(20)}";
    _confirmDialog.Q<Label>("DateLabel").text = $"Date: {PendingReservation.TicketDate.ToShortDateString()}";
    _confirmDialog.Q<Label>("TimeLabel").text = $"Time: {PendingReservation.TicketTime:h\\:mm}";
    _confirmDialog.Q<Label>("SeatLabel").text = $"Seat: {PendingReservation.SeatId}";
  }
  //=====================================================================================================
  /// <summary>
  /// Submit reservation and return to the main screen.
  /// </summary>
  private void SubmitReservation()
  {
    Log.Info($"Reservation Submitted, movie:{PendingReservation.MovieId}, date:{PendingReservation.TicketDate}, seat:{PendingReservation.SeatId}");

    MovieService.Instance.MakeReservation(PendingReservation.MovieId, PendingReservation.TicketDate, PendingReservation.TicketTime, PendingReservation.SeatId);
    _confirmDialog.style.display = DisplayStyle.None;
    OnProfileClicked();
    Messenger<string>.Broadcast(AppEvents.ChangeHeader, "Working...");
  }
  //----------------------------------------------------------------------------------------------------
  private void CancelReservation()
  {
    _confirmDialog.style.display = DisplayStyle.None;
  }
  //=====================================================================================================
  public void ShowLoadingDialog()
  {
    _loadingDialog.style.display = DisplayStyle.Flex;
    _loadingDialog.BringToFront();
  }
  //----------------------------------------------------------------------------------------------------
  public void HideLoadingDialog()
  {
    _loadingDialog.style.display = DisplayStyle.None;
  }
  //=====================================================================================================
  /// <summary>
  /// Show the Loading dialog then update the reservations. Once resz are updated, show the actual seats screen.
  /// </summary>
  public void ShowSeats()
  {
    ShowLoadingDialog();
    MovieService.Instance.GetReservations(AppEvents.ShowSeats);
  }
  //----------------------------------------------------------------------------------------------------
  /// <summary>
  /// Show the seats screen once the resz are loaded. This runs from a response to the AppEvents.ShowSeats message.
  /// </summary>
  private void ShowSeatsScreen()
  {
    HideLoadingDialog();
    SeatsController seats = new SeatsController();
    seats.Show(_root, _seatsScreenTemplate, _seatTemplate);
  }
}
