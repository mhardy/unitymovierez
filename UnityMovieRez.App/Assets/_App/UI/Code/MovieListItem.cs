using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using System.Linq;
using UnityMovieRez.Tools;

public class MovieListItem
{
  public bool HasReserved = false;

  private Label _title;
  private Label _overview;
  private VisualElement _poster;
  private VisualElement _body;

  private const int _maxOverviewLength = 200;
  //=====================================================================================================
  public void SetRoot(VisualElement root)
  {
    _title = root.Q<Label>("Title");
    _overview = root.Q<Label>("Overview");
    _poster = root.Q<VisualElement>("Poster");
    _body = root.Q<VisualElement>("CardBody");
  }
  //=====================================================================================================
  public void SetMovie(Movie movie)
  {
    // flag this movie if we've already made a reservation for it
    if( MovieService.Instance.Reservations.Where(a => a.MovieId == movie.id && a.PlayerId == AuthenticationManager.Instance.PlayerId).Count() > 0 )
    {
      movie.Reserved = true;
      _body.AddToClassList("movie-reserved");
    }
    else
    {
      _body.RemoveFromClassList("movie-reserved");
    }

    _title.text = movie.title.Shorten(20);
    if( movie.overview != null )
      _overview.text = movie.overview.Shorten(_maxOverviewLength);
    else
      _overview.text = "";

    _poster.style.backgroundImage = new StyleBackground(movie.Poster);
  }
}
