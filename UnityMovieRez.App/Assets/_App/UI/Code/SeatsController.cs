using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityMovieRez.Tools;

public class SeatsController
{
  private VisualTreeAsset _seatsScreenTemplate;
  private VisualTreeAsset _seatTemplate;

  private ListView _movieList;
  private VisualElement _footer;

  //=====================================================================================================
  public void Show(VisualElement root, VisualTreeAsset seatsScreenTemplate, VisualTreeAsset seatTemplate)
  {
    _movieList = root.Q<ListView>("MovieList");
    _footer = root.Q<VisualElement>("Footer");
    _seatsScreenTemplate = seatsScreenTemplate;
    _seatTemplate = seatTemplate;

    // switch to seat screen
    var body = _movieList.parent; // Body
    body.Remove(_movieList);
    body.Remove(_footer);

    var seatsScreen = _seatsScreenTemplate.Instantiate();
    seatsScreen.name = "SeatsScreen";
    body.Add(seatsScreen);
    var seatsList = seatsScreen.Q<VisualElement>("SeatList");

    // retrieve how many seats this movie has
    var movie = MovieService.Instance.GetPendingMovie();
    int seats = 55;
    if( movie != null && movie.seats != null )
      seats = (int)movie.seats;

    Log.Info($"SeatsController.Show, movie:{movie.title}, seats:{seats}");

    for( int i = 1; i <= seats; i++ )
    {
      var seat = _seatTemplate.Instantiate();
      var component = new SeatItem();
      seat.userData = component;
      component.Set(seat, i);

      seatsList.Add(seat);
    }
  }


}
