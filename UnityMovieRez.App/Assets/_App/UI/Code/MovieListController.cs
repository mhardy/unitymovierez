using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UIElements;
using UnityMovieRez.Tools;
using System.Linq;

public class MovieListController
{
  private ListView _movieList;
  private VisualTreeAsset _listItemTemplate;
  private Button _showTime7;
  private Button _showTime830;
  private Button _showTime9;
  private VisualElement _footer;

  private List<Movie> _movies;
  VisualElement _root;

  //=====================================================================================================
  public void Initialize(VisualElement root, VisualTreeAsset listItemTemplate)
  {
    Log.Info($"MovieListController, movies:{MovieService.Instance.Movies.Count}");

    _movies = MovieService.Instance.Movies;
    _root = root;

    //var l = new Label(AuthenticationManager.Instance.PlayerId); // for debugging
    var l = new VisualElement();
    _root.Add(l);

    _listItemTemplate = listItemTemplate;
    _movieList = root.Q<ListView>("MovieList");
    _showTime7 = root.Q<Button>("ShowTime7");
    _showTime830 = root.Q<Button>("ShowTime830");
    _showTime9 = root.Q<Button>("ShowTime9");
    _footer = root.Q<VisualElement>("Footer");

    if( _movieList == null )
    {
      Log.Error("NULL MovieList");
    }

    FillList();

    _movieList.onSelectionChange += OnMovieClicked;
    _showTime7.clicked += OnShowTime7Clicked;
    _showTime830.clicked += OnShowTime830Clicked;
    _showTime9.clicked += OnShowTime9Clicked;

    // disable until they select a movie
    _footer.SetEnabled(false);
  }
  //=====================================================================================================
  private void OnShowTime7Clicked()
  {
    Log.Info($"OnShowTime7Clicked, parent:{_movieList.parent.name}");
    OnShowTimeClicked(new TimeSpan(7, 0, 0));
  }
  //----------------------------------------------------------------------------------------------------
  private void OnShowTime830Clicked()
  {
    Log.Info($"OnShowTime830Clicked");
    OnShowTimeClicked(new TimeSpan(8, 30, 0));
  }
  //----------------------------------------------------------------------------------------------------
  private void OnShowTime9Clicked()
  {
    Log.Info($"OnShowTime9Clicked");
    OnShowTimeClicked(new TimeSpan(9, 0, 0));
  }
  //----------------------------------------------------------------------------------------------------
  private void OnShowTimeClicked(TimeSpan ticketTime)
  {
    Messenger<string>.Broadcast(AppEvents.ChangeHeader, $"Select a Seat\r\nfor {UIController.Instance.PendingReservation.TicketDate:d}");
    UIController.Instance.PendingReservation.TicketTime = ticketTime;
    UIController.Instance.ShowSeats();
  }

  //=====================================================================================================
  private void FillList()
  {
    Log.Info($"MovieListController, FillList, movies:{_movies.Count}");

    try
    {
      // Setup a new list item
      _movieList.makeItem = () =>
      {
        var newItem = _listItemTemplate.Instantiate();
        var component = new MovieListItem();

        newItem.userData = component;
        component.SetRoot(newItem);
        Log.Info($"-makeItem");

        return newItem;
      };

      // Set up bind function for the item
      _movieList.bindItem = (item, index) =>
      {
        if( index >= _movies.Count )
        {
          Log.Error($"Invalid movie index:{index}, movie count:{_movies.Count}");
          return;
        }
        Log.Info($"bindItem, movieId:{_movies[index].id}");

        (item.userData as MovieListItem).SetMovie(_movies[index]);
      };

      _movieList.fixedItemHeight = 220;
      _movieList.itemsSource = _movies;
    }
    catch( Exception ex )
    {
      Log.Error($"FillList", ex);
    }
  }
  //=====================================================================================================
  /// <summary>
  /// If the current user already has a res for this movie, take them to the seating chart for their
  /// scheduled time. Otherwise, enable the footer allow them to pick a time.
  /// </summary>
  /// <param name="selectedItems"></param>
  private void OnMovieClicked(IEnumerable<object> selectedItems)
  {
    var movie = _movieList.selectedItem as Movie;
    Log.Info($"index:{_movieList.selectedIndex}, reserved?{movie.Reserved}");

    // if nothing is selected
    if( movie == null )
    {
      _footer.SetEnabled(false);
    }
    else
    {
      UIController.Instance.PendingReservation.MovieId = movie.id;

      // see if the current user has a rez for this movie yet
      var res = MovieService.Instance.Reservations.Where(a => a.MovieId == movie.id && a.PlayerId == AuthenticationManager.Instance.PlayerId).FirstOrDefault();

      // if they've already got a res, go directly to show them the seating chart
      if( res != null )
      {
        // set the pending res with this users date/time so we show the seating chart for that specific showing
        Log.Info($"OnMovieClicked, resDate:{UIController.Instance.PendingReservation.TicketDate}");
        UIController.Instance.PendingReservation.TicketDate = res.TicketDate;
        UIController.Instance.PendingReservation.TicketTime = res.TicketTime;
        UIController.Instance.ShowSeats();
        Messenger<string>.Broadcast(AppEvents.ChangeHeader, $"{movie.title.Shorten(20)}\r\nYour {res.TicketTime:h\\:mm} Showing\r\non {res.TicketDate:d}");
      }
      else
      {
        _footer.SetEnabled(true);
        Messenger<string>.Broadcast(AppEvents.ChangeHeader, "Select a Show Time");
      }
    }
  }
}
