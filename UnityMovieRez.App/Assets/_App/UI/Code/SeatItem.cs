using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityMovieRez.Tools;
using System.Linq;

public class SeatItem
{
  private int _seatId;
  private Button _seatButton;
  
  private bool _isReserved;
  private bool _isReservedByPlayer;

  //=====================================================================================================
  public void Set(VisualElement root, int seatId)
  {
    _seatId = seatId;
    _seatButton = root.Q<Button>("SeatButton");
    _seatButton.text = _seatId.ToString();
    _seatButton.clicked += OnSeatClicked;

    // see if this seat is reserved for this movie & date/time
    var pendingRes = UIController.Instance.PendingReservation; // pull the current movie & date/time from the pending res
    Log.Info($"Seat.Set {_seatId}, movie:{pendingRes.MovieId}, seat:{pendingRes.SeatId}, date:{pendingRes.TicketDate.Date}, time:{pendingRes.TicketTime}");
    
    var res = MovieService.Instance.Reservations.Where(a => 
      a.MovieId == pendingRes.MovieId &&
      a.TicketDate.Date == pendingRes.TicketDate.Date &&
      a.TicketTime == pendingRes.TicketTime && 
      a.SeatId == _seatId).FirstOrDefault();
    if( res != null )
    {
      _isReserved = true;
      _isReservedByPlayer = res.PlayerId == AuthenticationManager.Instance.PlayerId;

      _seatButton.AddToClassList($"seat-reserved{(_isReservedByPlayer ? "-player" : "")}");
      
      Log.Info($"Seat.Set {_seatId}, ADDING class");
    }
    else
    {
      Log.Info($"Seat.Set {_seatId}, removing classes");

      _seatButton.RemoveFromClassList("seat-reserved");
      _seatButton.RemoveFromClassList("seat-reserved-player");
    }
  }
  //=====================================================================================================
  private void OnSeatClicked()
  {
    Log.Info($"Seat.OnClicked {_seatId}. reserved:{_isReserved}, resByPlayer:{_isReservedByPlayer}, selectedDate:{UIController.Instance.SelectedDate}");

    // can't click on any seat (reserved or not) if they've already got a res for this movie.
    if( MovieService.Instance.Reservations.Where(a => a.MovieId == UIController.Instance.PendingReservation.MovieId && a.PlayerId == AuthenticationManager.Instance.PlayerId).Count() > 0 )
      return;
    
    UIController.Instance.PendingReservation.SeatId = _seatId;
    // update date/time in case we were last viewing a seating chart for a reserved movie
    UIController.Instance.PendingReservation.TicketDate = UIController.Instance.SelectedDate;
    UIController.Instance.ShowConfirmDialog();
  }
}
