using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UIElements;
using UnityMovieRez.Tools;

public class MainScreen : MonoBehaviour
{
  [SerializeField] private VisualTreeAsset _listItemTemplate;

  //=====================================================================================================
  async Task OnEnable()
  {
    Log.Info($"MainScreen.OnEnable");

    await WaitForMoviesToLoad();

    var uiDocument = GetComponent<UIDocument>();
    var listController = new MovieListController();
    listController.Initialize(uiDocument.rootVisualElement, _listItemTemplate);
  }
  //=====================================================================================================
  /// <summary>
  /// Spin for a bit until the movies are loaded.
  /// </summary>
  /// <returns></returns>
  private async Task WaitForMoviesToLoad()
  {
    while( !MovieService.Instance.IsLoaded )
    {
      Log.Info($"MovieListController, Wait, movies:{MovieService.Instance.Movies.Count}");
      await Task.Delay(200);
    }
    UIController.Instance.HideLoadingDialog();
  }

}
