SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Movie](
	[id] [int] NOT NULL,
	[title] [nvarchar](1000) NULL,
	[overview] [nvarchar](max) NULL,
	[popularity] [decimal](8, 3) NULL,
	[poster_path] [varchar](200) NULL,
	[seats] [int] NULL,
 CONSTRAINT [PK_Movie] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


