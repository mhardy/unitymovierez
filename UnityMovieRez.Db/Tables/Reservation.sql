SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Reservation](
	[ReservationId] [int] IDENTITY(1,1) NOT NULL,
	[PlayerId] [varchar](50) NOT NULL,
	[MovieId] [int] NOT NULL,
	[TicketDate] [date] NOT NULL,
	[TicketTime] [time](7) NOT NULL,
	[SeatId] [int] NOT NULL,
	[DateReserved] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Reservation] PRIMARY KEY CLUSTERED 
(
	[ReservationId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Reservation] ADD  CONSTRAINT [DF_Reservation_DateReserved]  DEFAULT (getutcdate()) FOR [DateReserved]
GO


