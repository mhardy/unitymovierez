# UnityMovieRez (UMR)

UMR is a simple but functional movie reservation system backed by an Azure SQL Database and multiple Azure Functions.  The Functions provide an API layer between the Unity clients and the database.  It uses the Unity Game Services Authentication package with Anonymous authentication to maintain user state with a PlayerId.

Movie poster images are downloaded from the TMDB API when the app is first launched, then cached to disk for use on subsequent runs.

The overall design was geared towards simplicity for the user to make a reservation with as few steps as possible.  To make a reservation:

- Select your date from the date picker in the upper left
- Select your movie from the scrollable list
    - this will enable show times
- Click a show time at the bottom
    - this will bring you to the seat reservation page
- Click a seat and verify your selection
- Your reservation is complete and the UI will update to reflect your new reservation

## Features & Requirements

- You must be connected to the internet as it is dependent on the custom UMR API & the TMDB API to retrieve movies, reservations and images.
- If you have reservations, a badge in the top right corner on the Profile icon will display how many.
- Click the Profile icon to go back to the movie list at any time.
- Any movie you have a reservation for will have an orange background and will not allow a new reservation for that movie.
- Clicking a movie you currently have a reservation for will show you the seating chart for the showing for *your* date and time (not the selected date).
- Seats will show blue if they're available, grey if they're reserved by someone else, orange if it's your reservation.
- Each Movie has a different number of available seats.
- Reservations are reloaded when accessing the seats page so any new reservations since app start are reflected.

## Known Issues

- Safe Area isn't being accounted for so the header might get cropped on some devices.
- Unknown type: 'Unity.UI.Builder.UnityUIBuilderSelectionMarker' on windows build.
- ~~If your device time or timezone is not set to local time a reservation could be made for the wrong day.~~ (fixed in UMR.v.02.apk)

## Builds

**Android**: APK file that can be used to install `UMR` on Android devices.

**Windows**: Zip containing files to run on Windows.

To download from bitbucket, click the binary file, then click 'View raw'.

# Projects

The following projects built in Visual Studio and Unity make up the UMR system.

## UnityMovieRez.Api

Azure functions used to communicate with the Unity clients and the reservation database. This API handles all the functionality of retrieving movies and making reservations.

It also has a timer job that updates the movie database from the TMDB API every 30 minutes.

### Functions

The base endpoint for the API is:

`https://unitymovierezapi.azurewebsites.net/api/`

Follow `/api/` with the REST API function to call.  The API is secured with an API key that must be appended to the url using `?code=<api key>`

**AddMovies (Timer)**

Runs every 30 minutes and adds the top 20 currently playing movies to the database.

**GetAllMovies (HTTP:Get)**

Returns all  the movies in the database.

**GetReservationsByPlayer (HTTP:Get)**

Returns all the reservations for a specific player.

**GetReservationsByMovie (HTTP:Get)**

Returns all the reservations for a movie.

**GetAllReservations (HTTP:Get)**

Returns all the reservations in the system.

**MakeReservation (HTTP:Put)**

Call to make a reservation.

## UnityMovieRez.App

Unity client app for Android mobile devices and Windows desktop.

** Note: WebGL doesn't work due to CORS errors from the GameServices Auth API.

## UnityMovieRez.Tools

DLL for logging. Wrapping Debug.Log allows for finer control over logging. Having it in it's own DLL let's you jump directly to the logged line, rather than the log wrapper line.

This copies itself to the .App\Assets\Plugins directory during a post build event.

## UnityMovieRez.Db

DDL for database used to store movies and reservations.

