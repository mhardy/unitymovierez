﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using UnityMovieRez.Api.Data;
using UnityMovieRez.Api.Data.Models.Entities;

namespace UnityMovieRez.Api.Services
{
  public class MovieService
  {
    private readonly HttpClient _httpClient;
    private readonly Db _db;
    private readonly string _baseUrl = "https://api.themoviedb.org/3/movie/{0}?api_key=17e645432bbacaf4e4d5063197a3d0d8";

    //=====================================================================================================
    public MovieService()
    {
      _httpClient = new HttpClient();
      _db = new Db();
    }
    //=====================================================================================================
    /// <summary>
    /// Call themoviedb.org's API for the first 20 movies and save them to the database.
    /// </summary>
    /// <returns></returns>
    public async Task GetMovies()
    {
      string url = string.Format(_baseUrl, "now_playing") + "&region=US";
      var response = await _httpClient.GetAsync(url);

      if( response.StatusCode == System.Net.HttpStatusCode.OK )
      {
        var json = await response.Content.ReadAsStringAsync();
        var movies = JsonConvert.DeserializeObject<MovieList>(json);

        foreach( var movie in movies.results )
        {
          Log.Info($"Adding movie id:{movie.id}, title:{movie.title}");
          _db.AddMovie(movie.id, movie.title, movie.overview, (decimal)movie.popularity, movie.poster_path);
        }
      }

    }

  }
}
