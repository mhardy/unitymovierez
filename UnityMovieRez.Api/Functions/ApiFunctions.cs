﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using UnityMovieRez.Api.Data;
using UnityMovieRez.Api.Data.Models.Entities;

namespace UnityMovieRez.Api.Functions
{
  public static class ApiFunctions
  {
    //=====================================================================================================
    [FunctionName("GetMovie")]
    public static async Task<IActionResult> GetMovie(
        [HttpTrigger(AuthorizationLevel.Function, "get", Route = null)] HttpRequest req,
        ILogger log)
    {
      log.LogInformation("GetMovie function processed a request.");

      string mid = req.Query["id"];
      if( !int.TryParse(mid, out int movieId) )
        return new BadRequestObjectResult($"Unable to determine movieId from id:{mid}.");

      var db = new Db();
      var movie = await db.GetMovieAsync(movieId);

      return new OkObjectResult(movie);
    }
    //=====================================================================================================
    [FunctionName("GetAllMovies")]
    public static async Task<IActionResult> GetAllMovies(
        [HttpTrigger(AuthorizationLevel.Function, "get", Route = null)] HttpRequest req,
        ILogger log)
    {
      log.LogInformation("GetAllMovies function processed a request.");

      var db = new Db();
      var movies = await db.GetMoviesAsync();

      return new OkObjectResult(movies);
    }
    //=====================================================================================================
    [FunctionName("GetAllReservations")]
    public static async Task<IActionResult> GetAllReservations(
        [HttpTrigger(AuthorizationLevel.Function, "get", Route = null)] HttpRequest req,
        ILogger log)
    {
      log.LogInformation("GetAllMovies function processed a request.");

      var db = new Db();
      var resz = await db.GetReservationsAsync();

      return new OkObjectResult(resz);
    }
    //=====================================================================================================
    [FunctionName("GetReservationsByMovie")]
    public static async Task<IActionResult> GetReservationsByMovie(
        [HttpTrigger(AuthorizationLevel.Function, "get", Route = null)] HttpRequest req,
        ILogger log)
    {
      log.LogInformation("GetReservations function processed a request.");

      string mid = req.Query["movieId"];
      string tm = req.Query["time"];
      if( !int.TryParse(mid, out int movieId) || !TimeSpan.TryParse(tm, out TimeSpan time) )
        return new BadRequestObjectResult($"Unable to determine movieId or time from movieId:{mid}, time:{tm}.");

      var db = new Db();
      var res = await db.GetReservationsAsync(movieId, time);

      if( res == null )
        return new BadRequestObjectResult("Unable to retrieve reservations by movie.");

      string response = JsonConvert.SerializeObject(res);

      return new OkObjectResult(response);
    }
    //=====================================================================================================
    [FunctionName("GetReservationsByPlayer")]
    public static async Task<IActionResult> GetReservationsByPlayer(
        [HttpTrigger(AuthorizationLevel.Function, "get", Route = null)] HttpRequest req,
        ILogger log)
    {
      log.LogInformation("GetReservations function processed a request.");

      string playerId = req.Query["playerId"];
      if( string.IsNullOrEmpty(playerId) )
        return new BadRequestObjectResult($"Unable to determine playerId.");

      var db = new Db();
      var res = await db.GetReservationsAsync(playerId);

      if( res == null )
        return new BadRequestObjectResult("Unable to retrieve reservations by player.");

      string response = JsonConvert.SerializeObject(res);

      return new OkObjectResult(response);
    }
    //=====================================================================================================
    [FunctionName("MakeReservation")]
    public static async Task<IActionResult> MakeReservation(
        [HttpTrigger(AuthorizationLevel.Function, "put", Route = null)] HttpRequest req,
        ILogger log)
    {
      log.LogInformation("MakeReservation function processed a request.");

      string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
      Reservation data = JsonConvert.DeserializeObject<Reservation>(requestBody);

      log.LogInformation($"data:{data}");

      var db = new Db();
      var res = await db.MakeReservationAsync(data.MovieId, data.PlayerId, data.TicketDate, data.TicketTime, data.SeatId);

      return new OkObjectResult(res);
    }

  }
}
