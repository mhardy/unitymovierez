using System;
using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using UnityMovieRez.Api.Services;
using Microsoft.EntityFrameworkCore;
using UnityMovieRez.Api.Data;
using System.Threading.Tasks;

namespace UnityMovieRez.Api.Functions
{
  //=====================================================================================================
  public static class TimerFunctions
  {
    //=====================================================================================================
    /// <summary>
    /// Call TMDB and add the first 20 movies to the database.
    /// </summary>
    /// <param name="myTimer"></param>
    /// <param name="log"></param>
    /// <returns></returns>
    [FunctionName("AddMovies")]
    public static async Task AddMovies([TimerTrigger("0 */30 * * * *")] TimerInfo myTimer, ILogger log) // {second} {minute} {hour} {day} {month} {day-of-week}
    {
      // every hour for now, not sure how many api calls I can make...
      log.LogInformation($"Movie timer trigger function executed at: {DateTime.Now}");

      MovieService ms = new MovieService();
      await ms.GetMovies();
    }
  }
}
