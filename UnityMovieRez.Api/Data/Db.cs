﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using UnityMovieRez.Api.Data.Models.Entities;
using UnityMovieRez.Api.Services;
using System.Linq;

namespace UnityMovieRez.Api.Data
{
  public class Db
  {
    //=====================================================================================================
    private MovieDbContext GetContext()
    {
      //Environment.SetEnvironmentVariable("SqlConnection", "...");

      string connectionString = Environment.GetEnvironmentVariable("SqlConnection");
      Log.Info($"connectionString:{connectionString}");
      var options = new DbContextOptionsBuilder<MovieDbContext>().UseSqlServer(connectionString).Options;
      return new MovieDbContext(options);
    }
    //=====================================================================================================
    public void AddMovie(int movieId, string title, string overview, decimal popularity, string posterPath)
    {
      using( var db = GetContext() )
      {
        Movie movie = db.Movie.Find(movieId);
        if( movie == null )
        {
          movie = new Movie();
          db.Movie.Add(movie);
        }

        movie.id = movieId;
        movie.title = title;
        movie.overview = overview;
        movie.popularity = popularity;
        movie.poster_path = posterPath;
        Random r = new Random();
        movie.seats = 50 + r.Next(1, 20);

        db.SaveChanges();
      }
    }
    //=====================================================================================================
    public async Task<List<Movie>> GetMoviesAsync()
    {
      using( var db = GetContext() )
      {
        return await db.Movie.ToListAsync();
      }
    }
    //=====================================================================================================
    public async Task<Movie> GetMovieAsync(int movieId)
    {
      using( var db = GetContext() )
      {
        return await db.Movie.Where(a => a.id == movieId).FirstOrDefaultAsync();
      }
    }
    //=====================================================================================================
    public async Task<List<Reservation>> GetReservationsAsync()
    {
      using( var db = GetContext() )
      {
        return await db.Reservation.ToListAsync();
      }
    }
    //=====================================================================================================
    public async Task<List<Reservation>> GetReservationsAsync(int movieId, TimeSpan time)
    {
      using( var db = GetContext() )
      {
        return await db.Reservation.Where(a => a.MovieId == movieId && a.TicketTime == time).ToListAsync();
      }
    }
    //=====================================================================================================
    public async Task<List<Reservation>> GetReservationsAsync(string playerId)
    {
      using( var db = GetContext() )
      {
        return await db.Reservation.Where(a => a.PlayerId == playerId).ToListAsync();
      }
    }
    //=====================================================================================================
    /// <summary>
    /// Return PostResponse with the ReservationId if the reservation was made successfully, else return the reason it could not be made.
    /// </summary>
    public async Task<PostResponse> MakeReservationAsync(int movieId, string playerId, DateTime date, TimeSpan time, int seatId)
    {
      using( var db = GetContext() )
      {
        try
        {
          Reservation res = db.Reservation.Where(a => a.MovieId == movieId && a.PlayerId == playerId).FirstOrDefault();
          // TODO: currently just matching for user & movie, not times...

          if( res != null )
          {
            return new PostResponse { IsError = true, Message = "A reservation for this user and movie has already been made" };
          }

          res = new Reservation
          {
            MovieId = movieId,
            PlayerId = playerId,
            TicketDate = date,
            TicketTime = time,
            SeatId = seatId
          };
          db.Reservation.Add(res);

          await db.SaveChangesAsync();
          return new PostResponse { Id = res.ReservationId };
        }
        catch( Exception ex )
        {
          return new PostResponse { IsError = true, Message = $"{ex.Message}, inner:{ex.InnerException?.Message}" };
        }
      }
    }
  }
}

