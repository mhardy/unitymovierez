﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace UnityMovieRez.Api.Data.Models.Entities
{
    public partial class Reservation
    {
        public int ReservationId { get; set; }
        public string PlayerId { get; set; }
        public int MovieId { get; set; }
        public DateTime TicketDate { get; set; }
        public TimeSpan TicketTime { get; set; }
        public int SeatId { get; set; }
        public DateTime DateReserved { get; set; }
    }
}
