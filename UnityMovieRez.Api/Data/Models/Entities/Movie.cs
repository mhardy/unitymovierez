﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace UnityMovieRez.Api.Data.Models.Entities
{
    public partial class Movie
    {
        public int id { get; set; }
        public string title { get; set; }
        public string overview { get; set; }
        public decimal? popularity { get; set; }
        public string poster_path { get; set; }
        public int? seats { get; set; }
    }
}
