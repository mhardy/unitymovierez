﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnityMovieRez.Api.Data.Models.Entities
{
  public class MovieList
  {
    public int page;
    public List<Movie> results;
  }
}
