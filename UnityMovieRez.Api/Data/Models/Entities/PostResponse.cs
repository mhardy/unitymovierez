﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnityMovieRez.Api.Data.Models.Entities
{
  public class PostResponse
  {
    public bool IsError = false;
    public string Message;
    public int Id;
  }
}
