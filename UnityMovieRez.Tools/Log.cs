﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace UnityMovieRez.Tools
{
  /// <summary>
  /// Very simply logging wrapper.  It's in it's own DLL so when you click a log messages it goes to the
  /// line that called it, not to the log wrapper code itself.
  /// Using a wrapper allows us much finer control over logging. e.g. sending anaytics events during errors.
  /// This copies itself to the .App\Assets\Plugins directory during a post build event.
  /// </summary>
  public static class Log
  {
    //=====================================================================================================
    public static void Info(string message)
    {
      Debug.Log(message);
    }
    //=====================================================================================================
    public static void Error(string message, Exception ex = null)
    {
      Debug.LogError($"ERROR: {message}, Exception:{ex?.Message}");
    }
  }
}